			{ [14CH3] }
------------------------------------------------------------------------------------------	Ce fichier contient le nom et Smiles des polymères constitués de Carbone14, pas supporté par rBAN donc remplacé par du carbone normal.
------------------------------------------------------------------------------------------

146034282 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3]
146034282 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3]
146034282 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3])C(C)C)CCCNC(=O)N)[14CH3]
146033421 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)COC)[14CH3])C(C)C)COC)[14CH3])C(C)C)COC)[14CH3]
146033421 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)COC)[14CH3])C(C)C)COC)[14CH3])C(C)C)COC)[14CH3]
146033421 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)COC)[14CH3])C(C)C)COC)[14CH3])C(C)C)COC)[14CH3]
146029290 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCSC)[14CH3])C(C)C)CCSC)[14CH3])C(C)C)CCSC)[14CH3]
146029290 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCSC)[14CH3])C(C)C)CCSC)[14CH3])C(C)C)CCSC)[14CH3]
146029290 : CC(C)[C@H]1C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O[C@@H](C(=O)N([C@H](C(=O)O1)CCSC)[14CH3])C(C)C)CCSC)[14CH3])C(C)C)CCSC)[14CH3]
135889918 : [14CH3][14C@H]([14C](=O)O)N
